package tests;

import bases.TestBase;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pages.ClinicorpMainPage;
import pages.MainPage;
import pages.PacientesPage;
import utils.DriverFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class ClinicorpTests extends TestBase {

    @Test
    public void task1Test() throws InterruptedException, IOException {

        ClinicorpMainPage mainPage = new ClinicorpMainPage();
        PacientesPage pacientesPage = new PacientesPage();

        mainPage.login("amanda.gontijo@amandagontijoortodontia", "a071207h");

        pacientesPage.acessarPacientes();

        //pacientesPage.clicarAdicionarNovo();

        URL url = Thread.currentThread().getContextClassLoader().getResource("PlanilhaparaClinicorp.xlsx");
        FileInputStream file = new FileInputStream(new File(url.getPath()));
        Workbook workbook = new XSSFWorkbook(file);

        Sheet sheet = workbook.getSheetAt(0);

        int i = 0;

        for (Row row : sheet) {

            if (i == 0) {
                i++;
                continue; // skip first row
            } else {
                i++;
            }

            double id = row.getCell(0).getNumericCellValue();

            String nome = row.getCell(2).getRichStringCellValue().getString();

            if (nome.equals("")) {
                nome = "Sem Nome" + id;
            }

            // Data de Nascimento
            Date dataNascimento = row.getCell(9).getDateCellValue();

            if (dataNascimento == null)
                dataNascimento = new Date(100, 0, 1, 0, 0);


            int dia = dataNascimento.getDate();
            int mes = dataNascimento.getMonth() + 1;
            int ano = dataNascimento.getYear() + 1900;

            String cpf = row.getCell(11).getRichStringCellValue().getString();

            //int carteirinha = (int) Math.round(row.getCell(10).getNumericCellValue()) * 1000000;
            String carteirinha = row.getCell(14).getRichStringCellValue().getString();

            String observacoes = "Carteirinha: " + carteirinha;

            if (i > 2) {
                pacientesPage.clicarAdicionarNovo2();
            } else {
                pacientesPage.clicarAdicionarNovo();
            }

            pacientesPage.preencherPaciente(nome,
                    Integer.toString(dia),
                    Integer.toString(mes),
                    Integer.toString(ano),
                    observacoes);

            System.out.println(id + "; " + nome + "; " + dia + "/" + mes + "/" + ano + "; " + cpf + "; " + observacoes);

            Thread.sleep(2000);

            pacientesPage.clicarSalvar();

            Thread.sleep(4000);

        }

    }
}


