package pages;

import bases.PageBase;
import org.openqa.selenium.By;

public class ClinicorpMainPage extends PageBase {
    By loginButton   = By.xpath("//*[@id=\"myDIV\"]/div[2]/button");
    By usernameField = By.xpath("//*[@id=\"myDIV\"]/div[2]/div[2]/input");
    By passwordField = By.xpath("//*[@id=\"myDIV\"]/div[2]/div[3]/input");

    By pacientes = By.xpath("//*[@id=\"app\"]/div/div/div/div[3]/div[2]/a/div/h3");

    public void login(String username, String password) {

        this.click(usernameField);
        this.sendKeys(usernameField, username);
        this.click(passwordField);
        this.sendKeys(passwordField, password);
        this.click(loginButton);
    }

    public void acessarAgenda() {

        this.click(pacientes);
    }
}
