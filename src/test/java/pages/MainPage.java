package pages;

import bases.PageBase;
import utils.DriverFactory;
import utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MainPage extends PageBase {
    //By loginButton = By.linkText("Log In");
    By loginButton = By.linkText("Entrar");
    By usernameField = By.name("username");
    By passwordField = By.name("password");
    By loginButton2 = By.cssSelector(".auth0-label-submit");
    By pipeCandidatos = By.xpath(".//*[@id=\"reactPipesList\"]/div/div/div/div/div[1]/div[1]/a");
    By firstCard = By.xpath("/html/body/div[1]/div[2]/div[2]/div/ul/div[1]/li/div/div/div[1]");
    By cardTitle = By.id("edit-card-title");
    By cardHeaderMenu = By.id("open-card-header-menu");
    By deleteCardOption = By.id("delete-card-button");
    By confirmDeleteButton = By.xpath("/html/body/div[*]/div/div/div[2]/button[1]");

//*[@id="385751589"]/div[2]/p

//    By aboutUsLink = By.linkText("About us");
//    By faqLink = By.linkText("Faq");
//    By helpLink = By.id("Help");
//    By myAccountLink = By.id("My account");
//    By blogLink = By.id("Blog");
//    By contactsLink = By.id("Contacts");



    public void login(String username, String password) {

        this.click(loginButton);
        this.click(usernameField);
        this.sendKeys(usernameField, username);
        this.click(passwordField);
        this.sendKeys(passwordField, password);
        this.click(loginButton2);
        this.click(pipeCandidatos);
    }

    public void openFirstCard() {


        this.click(firstCard);
    }

    public String getCardTitle() {
        return this.getText(cardTitle);
    }

    public void deleteCard() {
        this.click(cardHeaderMenu);
        this.click(deleteCardOption);
        this.click(confirmDeleteButton);
    }


}
