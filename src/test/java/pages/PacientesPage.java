package pages;

import bases.PageBase;
import org.openqa.selenium.By;

public class PacientesPage extends PageBase {

    By pacientes = By.xpath("//*[@id=\"app\"]/div/div/div/div[3]/div[2]/a/div/h3");
    By adicionarNovoButton = By.xpath("//*[@id=\"ControlContent\"]/div[1]/div/button/div[2]/span");

    By adicionarNovoButton2 = By.xpath("//*[@id=\"ControlContent\"]/div[1]/div/div[2]/div/button/div[2]/span");

    By nomeField = By.xpath("//*[@id=\"Name\"]");

    By cancelarButton = By.xpath("/html/body/div[7]/div[2]/div/div/div[3]/div/button[1]");

    By salvarButton = By.xpath("/html/body/div[7]/div[2]/div/div/div[3]/div/button[2]");

    By diaSelect = By.xpath("//*[@id=\"BirthDate\" and @placeholder=\"Dia\"]");

    By mesSelect = By.xpath("//*[@id=\"BirthDate\" and @placeholder=\"Mês\"]");

    By anoSelect = By.xpath("//*[@id=\"BirthDate\" and @placeholder=\"Ano\"]");

    By observacaoField = By.xpath("//*[@id=\"Notes\"]");




    public void acessarPacientes() {

        this.click(pacientes);
    }

    public void clicarAdicionarNovo() {

        this.click(adicionarNovoButton);
    }

    public void clicarAdicionarNovo2() {

        this.click(adicionarNovoButton2);
    }

    public void preencherPaciente(String nome, String dia, String mes, String ano, String observacao) {

        this.sendKeys(nomeField, nome);
        this.comboBoxSelectByVisibleText(diaSelect, dia);
        this.comboBoxSelectByVisibleText(mesSelect, mes);
        this.comboBoxSelectByVisibleText(anoSelect, ano);
        this.sendKeys(observacaoField, observacao);
    }

    public void clicarCancelar() {

        this.click(cancelarButton);
    }

    public void clicarSalvar() {

        this.click(salvarButton);
    }
}
